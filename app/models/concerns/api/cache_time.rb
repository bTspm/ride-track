module Api
  module CacheTime
    def one_day
      86_400
    end

    def one_hour
      3_600
    end

    def one_minute
      60
    end
  end
end

