module Constants
  module Currency
    DEFAULT_TO   = 'GB'
    DEFAULT_FROM = 'US'

    DEFAULT_FROM_AND_TO = [DEFAULT_FROM, DEFAULT_TO]
  end
end

